"""
Project version info.
"""

__version_info__ = (0, 2, 0)
__version__ = '.'.join([int(num) for num in __version_info__])
