# -*- coding: utf-8 -*-
"""
Classes affiliated to the Natural Language Processing Pipeline.
"""

class NLPPipeline(object):
    """
    Natural Language Processing pipeline class. This class is used to process corpora when added to the system. Usually, processing consists of Named Entity recognition, Dependency Parsing and using those to do Open Relation Extraction, but more steps can also be added.
    """
    def __init__(self):
        pass
